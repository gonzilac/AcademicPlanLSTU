var store = {};


store.profileSelected = 0;
store.selectedOpop = 0;
store.selectedPlan = 0;

store.adminFgos=[];
store.adminProfiles=[];
store.selectedFgos={};
store.selectedProfile={};
store.adminOpop=[];
store.adminEform=[];
store.selectedOpopAdmin={};
var ServerName = "http://25.19.104.18:1313/";

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


function setCookie(name, value, options) {
    options = options || {};
    var expires = options.expires;
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}


function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}

function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};

function closePopup() {
    $('.cover-form .pop-form').css('transform', '');
    $('.cover-form').fadeOut(100);
    $('body').css({ 'overflow-y': '', 'padding-right': '' });
    return false;
}

$(document).on('click', '.openPopUp', function () {
    closePopup();
    $('body').css({ 'overflow-y': 'hidden', 'padding-right': getScrollBarWidth() });
    $($(this).attr('href')).fadeIn(100);
    $($(this).attr('href')).find('.pop-form').css('transform', 'scale(1)');
    return false;
});

$(document).on('click', '.cover-form', function (e) { if ($(e.target).hasClass('cover-form') || $(e.target).hasClass('close')) return closePopup(); });

var selectedRow;

$('.hamburger').click(function () {
    $(this).toggleClass('is-active');
    $('header').toggleClass('is-active');

    return false;
});


var dataCal = [
    [],
    [],
    [],
    [],
];
var selectRowCal;
var selectedItem = {

};



//Методы для компетенций



let token = getCookie('token');

if(token){
    store.token = token;
    riot.mount("selectedProfile");
}else{
    riot.mount("auth");
}